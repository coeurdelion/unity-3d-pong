﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{


    // public GameObject bulletPrefab;
    public Rigidbody rgbd;
    public float speed;
    public float time = -1;


    void Start()
    {
        Restart(2);
    }

    private void Update()
    {
        if (time > 0.0f)
        {
            time -= Time.deltaTime;
            if (time <= 0.0f)
            {
                Vector3 movement = new Vector3(10, 0, 2);
                rgbd.AddForce(movement * speed);
            }
        }
       
    }

    public void Restart(float t = 1.0f)
    {
        time = t;

        transform.position = new Vector3(0f, 1f, 0f);

        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        if (rb != null)
            rb.velocity = Vector3.zero;
    }

    // Non réussit
    public void Run()
    {
        if (time > 0.0f)
        {
            time -= Time.deltaTime;
            if (time <= 0.0f)
            {
                Vector3 movement = new Vector3(10, 0, 2);
                rgbd.AddForce(movement * speed);
            }
        }
    }
}
