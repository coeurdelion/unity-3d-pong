﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private float speed = 350f;
    public int player;
    private Rigidbody _rigidbody;
    private KeyCode _keyLeft;
    private KeyCode _keyRight;


    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        //if(isServer && isClient && id == 1)

        _keyLeft = player == 1 ? KeyCode.A : KeyCode.LeftArrow;
        _keyRight = player == 1 ? KeyCode.E : KeyCode.RightArrow;

    }
    // Update is called once per frame
    void Update() {

        if (Input.GetKey(_keyLeft))
        {

            _rigidbody.velocity = new Vector3(0f, 0f, speed * Time.deltaTime);
        }
        else if (Input.GetKey(_keyRight))
        {
            _rigidbody.velocity = new Vector3(0f, 0f, (speed * Time.deltaTime) * -1);
        } 
        else
        {
            _rigidbody.velocity = Vector3.zero;
        }
    }
        

        
    
}
