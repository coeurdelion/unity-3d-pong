﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGoal : MonoBehaviour
{


    public int id;
    public ScoreUI score;

	// Use this for initialization
	void OnCollisionEnter(Collision col) {
        
        Ball ball = col.gameObject.GetComponent<Ball>();
        if(ball != null)
        {
            // On comptabilise
            if (id == 1) score.scorePlayer2++;
            if (id == 2) score.scorePlayer1++;
            
            // Si le score de l'un des joueurs != 5 on remet la balle au centre et le jeu en pause
            if (score.scorePlayer2 < 5 || score.scorePlayer1 < 5)
            {
                ball.Restart();
            } 
            
        }
    }
	
}
